#!/usr/bin/with-contenv bash
# vi :set ft=bash:

cat > /etc/resolv.conf <<EOF
search local
nameserver 127.0.0.1
EOF
