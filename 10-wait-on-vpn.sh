#!/usr/bin/with-contenv bash
set -euo pipefail

echo "Waiting on VPN"
while ! ip r | grep 'dev tun0'>/dev/null; do
  echo -n '.'
done
echo "VPN up!"
