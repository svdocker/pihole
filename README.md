Extend [pihole/pihole](https://github.com/pi-hole/docker-pi-hole) by adding an `S6` script to run after `pihole` and replace the `/etc/resolv.conf` of the container.

Useful when the `pihole` container is included in a `network_mode: service:<service>` `docker-compose` configuration and thei `pihole` is to be the default DNS service.